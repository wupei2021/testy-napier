#!/bin/bash
touch tmp

if [ $# -eq 0 ]; then
    echo Sposób użycia komendy: bash szybciorem.sh nazwa_pliku.c nazwa_folderu_z_testami
    echo Nazwa folderu z testami jest opcjonalna - domyślnie /testy
    exit 1
fi

echo ---ROZPOCZYNAM TESTOWANIE---
echo Nazywa pliku projektowego: $1
echo
for file in ${2:-testy}/in/* ; do
    echo ---KOMPILACJA---
    echo Kompiluję z użyciem pliku programu testowego: $file
    echo
    gcc @opcje $file $1 -o project

    if [ $? -ne 0 ] ; then
        echo !-!-! BŁĄD KOMPILACJI !-!-!
        exit 1
    fi

    printf "Czas wykonania bez Valgrinda:"
    time(./project | diff - ${2:-testy}/out/$(basename -s .c $file).out)

    if [ $? -eq 0 ]; then
        printf "Pomyślnie ukończono próbę "$(basename -s .c $file)".\n"
        echo
    else
        echo !-!-! Błąd !-!-!
        echo Błędny wynik podczas wykonywania próby "$(basename $file)"
        echo Spodziewany wynik: 
        cat ${2:-testy}/out/$(basename -s .c $file).out
        echo Otrzymany wynik:
        valgrind --leak-check=full -q --error-exitcode=1 ./project
        break 1
    fi
done

echo ----CZYSZCZENIE----
echo Usuwam plik wykonywalny projektu
rm project
rm tmp
printf "\n"
echo ----ZAKOŃCZENIE PROCESU TESTOWANIA----
echo Dziękujemy za skorzystanie z programu testującego!