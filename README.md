# TESTY NAPIER***
Repozytorium będzie zawierać / zawiera testy do trzeciego projektu zaliczeniowego.

## Sposób kontrybucji testów
1. Utwórz swojego własnego forka i do niego commituj wszystkie swoje testy.
2. Wyślij merge-requesta aby Twoje testy zostały zatwierdzone.
3. Jeśli ktokolwiek potrafi ulepszyć skrypt to jest to jak najbardziej wskazane :)).
4. Testy mogą być przesyłane również do mnie (autor posta) na privie.

## Sposób uruchamiania skryptu testującego
1. Pobierz skrypt testujący do danego folderu.
2. Wrzuć plik programu z rozszerzeniem .c do folderu, gdzie znajduje się skrypt testujący.
3. Uruchom skrypt poleceniem powłoki: "bash testuj.sh nazwa_pliku.c" (bez "), gdzie nazwa_pliku to nazwa pliku z programem .c.
4. Urchomienie skryptu poleceniem powłowki: "bash szybciorem.sh nazwa_pliku.c" uruchamia szybkie testy BEZ valgrinda.
5. Wypisanie komendy bez żadnych argumentów np. "bash testuj.sh" wyświetla instrukcję użytkowania.

## Ważne informacje techniczne
1. Folder testy jest podzielony na programy testujące (będące testami) w języku .c oraz outputy wymagane.
2. WAŻNE! W opcjach jest dodana dodatkowa flaga "-I./" która nie ma wpływu na działanie kompilatora, ale ułatwia zastosowanie czystej struktury projektu. Informuje ona kompilator w jakim folderze ma on szukać "napiernaf.h".
3. TESTY MUSZĄ BYĆ PISANE POD LINUKSEM! (tylko takie gwarantują pełną ich poprawność)
4. Nie ponoszę odpowiedzialności za testowanie skryptem - dla mnie działa, więc powinien działać
5. Testy in .c i testy out .out muszą mieć tradycyjnie tą samą nazwę plikową.
