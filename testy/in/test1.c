#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "napiernaf.h"

void pisz(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%+d", a[i]);
    }
    putchar(';');
}

int main(void) {
    int jedenNN = 0;
    int *a, an;
    iton(INT_MAX, &a, &an);
    int *b, bn;
    iton(INT_MIN, &b, &bn);
    int *c, cn;
    nadd(a, an, &jedenNN, 1, &c, &cn);
    int *d, dn;
    nsub(b, bn, &jedenNN, 1, &d, &dn);
    int *e, en;
    nmul(a, an, b, bn, &e, &en);
    int *f, fn;
    nmul(e, en, NULL, 0, &f, &fn);
    pisz(a, an);
    printf(" (%d)\n", ntoi(a, an));
    pisz(b, bn);
    printf(" (%d)\n", ntoi(b, bn));
    pisz(c, cn);
    printf(" (%d)\n", ntoi(c, cn));
    pisz(d, dn);
    printf(" (%d)\n", ntoi(d, dn));
    pisz(e, en);
    printf(" (%d)\n", ntoi(e, en));
    pisz(f, fn);
    printf(" (%d)\n", ntoi(f, fn));
    free(a);
    free(b);
    free(c);
    free(d);
    free(e);
    free(f);
    return 0;
}
