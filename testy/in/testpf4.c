#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include "napiernaf.h"
#include <time.h>
#include <assert.h>

void wypisz(int a[], int n)
{
    for(int i=0 ; i<n ; i++)
    {
        printf("%2i ", a[i]);
    }
    printf(" | (%i)", ntoi(a, n));
    printf("\n");
}

int main(void)
{
    int *a = NULL;
    int *b = NULL;
    int an = 0;
    int bn = 0;
    int c = 1;
    int cn = 1;

    int *d;
    int dn;
    int *e;
    int en;
    nadd(a, an, b, bn, &d, &dn);
    assert(d == NULL);
    assert(dn == 0);

    nsub(a, an, b, bn, &d, &dn);
    assert(d == NULL);
    assert(dn == 0);

    nmul(a, an, &c, cn, &d, &dn);
    assert(d == NULL);
    assert(dn == 0);

    nmul(&c, cn, a, an, &d, &dn);
    assert(d == NULL);
    assert(dn == 0);

    nexp(a, an, &c, cn, &d, &dn);
    assert(d == NULL);
    assert(dn == 0);

    ndivmod(a, an, &c, cn, &d, &dn, &e, &en);
    assert(d == NULL);
    assert(dn == 0);
    assert(e == NULL);
    assert(en == 0);

    printf("GOOD\n");
    return 0;
}