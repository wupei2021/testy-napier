#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include "napiernaf.h"
#include <time.h>
#include "assert.h"

void wypisz(int a[], int n)
{
    for(int i=0 ; i<n ; i++)
    {
        printf("%2i ", a[i]);
    }
    printf(" | (%i)", ntoi(a, n));
    printf("\n");
}
void mergeNapier(int *a, int an, int *b, int bn, int **c, int *cn);
void wypiszNAF(int a[], int n)
{
    int dlugosc;
    if (a[n-1] < 0)
    {
        dlugosc = -(a[n-1] + 1) +1;
    }
    else
    {
        dlugosc = a[n-1] +1;
    }

    int *bbr = malloc(sizeof(int) * (size_t)dlugosc);

    for(int i=0 ; i<dlugosc ; i++)
    {
        bbr[i] = 0;
    }

    for(int i=0 ; i<n ; i++)
    {
        if (a[i] < 0)
        {
            bbr[-(a[i] + 1)] = -1;
        }
        else
        {
            bbr[a[i]] = 1;
        }
    }

    printf("BBR: ");
    for(int i=0 ; i<dlugosc ; i++)
    {
        printf("%i ", bbr[i]);
    }
    printf("\n");
    free(bbr);
}

int main(void)
{
    int *a;
    int an;
    int x;
    int c;
    srand((unsigned int) time(NULL));

    bool prawidlowo = true;
    for(int i=0 ; i<100000 ; i++)
    {
        x = rand();
        c = rand()%3-1; //LOSOWANIE ZNAKU
        if (c == 0) c = 1;
        iton(x*c, &a, &an);

        if (ntoi(a, an) != x*c) prawidlowo = false;
        free(a);
    }

    if (prawidlowo)
    {
        printf("UKOŃCZONO POMYŚLNIE!\n");
    }
    else
    {
        printf("Brak powodzenia dla liczby %i\n", x*c);
    }

    return 0;
}