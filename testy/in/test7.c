#include <stdio.h>
#include <stdlib.h>

#include "napiernaf.h"

void pisz(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%+d", a[i]);
    }
    putchar(';');
}

int main(void) {
    int *d, dn;
    iton(1, &d, &dn);
    int *t, tn;
    iton(1000000, &t, &tn);
    int *a, an;
    nexp(d, dn, t, tn, &a, &an);
    int *b, bn;
    nexp(NULL, 0, t, tn, &b, &bn);
    pisz(d, dn);
    printf(" (%d)\n", ntoi(d, dn));
    pisz(t, tn);
    printf(" (%d)\n", ntoi(t, tn));
    pisz(a, an);
    printf(" (%d)\n", ntoi(a, an));
    pisz(b, bn);
    printf(" (%d)\n", ntoi(b, bn));
    free(d);
    free(t);
    free(a);
    free(b);
    return 0;
}
