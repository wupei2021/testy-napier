#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include "napiernaf.h"
#include <time.h>
#include "assert.h"

void wypisz(int a[], int n)
{
    for(int i=0 ; i<n ; i++)
    {
        printf("%2i ", a[i]);
    }
    printf(" | (%i)", ntoi(a, n));
    printf("\n");
}

int main(void)
{
    int *a;
    int an;
    int *b;
    int bn;
    iton(INT_MAX, &a, &an);
    iton(2, &b, &bn);
    wypisz(a, an);
    wypisz(b, bn);

    int *c;
    int cn;
    nadd(a, an, b, bn, &c, &cn);
    wypisz(c, cn);
    free(c);

    nsub(a, an, b, bn, &c, &cn);
    wypisz(c, cn);
    free(a);
    free(b);
    free(c);
    return 0;
}