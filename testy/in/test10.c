#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "napiernaf.h"

void pisz(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%+d", a[i]);
    }
    putchar(';');
}

int main(void) {
    int minusJedenNN = -1;
    int *a, an;
    iton(1000000, &a, &an);
    int *b, bn;
    nmul(&minusJedenNN, 1, a, an, &b, &bn);
    int *c, cn;
    iton(90123, &c, &cn);
    int *d, dn;
    nmul(&minusJedenNN, 1, c, cn, &d, &dn);
    int *e, en;
    int *f, fn;
    ndivmod(a, an, c, cn, &e, &en, &f, &fn);
    int *g, gn;
    int *h, hn;
    ndivmod(a, an, d, dn, &g, &gn, &h, &hn);
    int *i, in;
    int *j, jn;
    ndivmod(b, bn, c, cn, &i, &in, &j, &jn);
    int *k, kn;
    int *l, ln;
    ndivmod(b, bn, d, dn, &k, &kn, &l, &ln);
    pisz(a, an);
    printf(" (%d)\n", ntoi(a, an));
    pisz(b, bn);
    printf(" (%d)\n", ntoi(b, bn));
    pisz(c, cn);
    printf(" (%d)\n", ntoi(c, cn));
    pisz(d, dn);
    printf(" (%d)\n", ntoi(d, dn));
    pisz(e, en);
    printf(" (%d)\n", ntoi(e, en));
    pisz(f, fn);
    printf(" (%d)\n", ntoi(f, fn));
    pisz(g, gn);
    printf(" (%d)\n", ntoi(g, gn));
    pisz(h, hn);
    printf(" (%d)\n", ntoi(h, hn));
    pisz(i, in);
    printf(" (%d)\n", ntoi(i, in));
    pisz(j, jn);
    printf(" (%d)\n", ntoi(j, jn));
    pisz(k, kn);
    printf(" (%d)\n", ntoi(k, kn));
    pisz(l, ln);
    printf(" (%d)\n", ntoi(l, ln));
    free(a);
    free(b);
    free(c);
    free(d);
    free(e);
    free(f);
    free(g);
    free(h);
    free(i);
    free(j);
    free(k);
    free(l);
}
