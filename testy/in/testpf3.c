#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include "napiernaf.h"
#include <time.h>
#include <assert.h>

void wypisz(int a[], int n)
{
    for(int i=0 ; i<n ; i++)
    {
        printf("%2i ", a[i]);
    }
    printf(" | (%i)", ntoi(a, n));
    printf("\n");
}

int main(void)
{
    int *a;
    int an;
    int *b;
    int bn;

    iton(1, &a, &an);
    iton(-1, &b, &bn);
    wypisz(a, an);
    wypisz(b, bn);

    int *c;
    int cn;
    nmul(a, an, b, bn, &c, &cn);
    wypisz(c, cn);
    assert(c != NULL);
    free(c);

    nadd(a, an, b, bn, &c, &cn);
    wypisz(c, cn);
    assert(c == NULL);
    free(c);

    nsub(a, an, b, bn, &c, &cn);
    wypisz(c, cn);
    free(c);

    nexp(b, bn, a, an, &c, &cn);
    wypisz(c, cn);
    free(c);

    free(b);
    iton(-8, &b, &bn);
    nexp(b, bn, a, an, &c, &cn);
    wypisz(c, cn);
    free(c);

    free(a);
    free(b);    

    return 0;
}